package com.example.willgunby.flickrbrowser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by willgunby on 03/04/15.
 */
public class BaseActivity extends ActionBarActivity{

    static final String FLICKR_QUERY = "FLICKR_QUERY";
    static final String PHOTO_TRANSFER = "";

    private Toolbar mToolbar;

    Toolbar activateToolbar(){
        if (mToolbar ==null){
            mToolbar = (Toolbar) findViewById(R.id.app_bar);
            if(mToolbar !=null) {
                setSupportActionBar(mToolbar);
            }
        }
        return mToolbar;
    }

    Toolbar activateToolbarWithHomeEnabled(){
        activateToolbar();
        if (mToolbar !=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        return mToolbar;
    }
}
