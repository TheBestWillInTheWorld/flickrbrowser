package com.example.willgunby.flickrbrowser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.willgunby.flickrbrowser.flickrinterface.FlickrItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by willgunby on 03/04/15.
 */
class RecyclerViewAdaptor extends RecyclerView.Adapter<ImageViewHolder> {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private List<FlickrItem> mPhotoList;
    private final Context mContext;

    public RecyclerViewAdaptor(Context context) {
        this.mContext = context;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse, null);

        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        FlickrItem item = mPhotoList.get(position);
        Log.d(LOG_TAG, "Processing: "+ item.getAuthor() +" - "+ item.getTitle() +" - "+ item.getImage() +" ("+ position +")");
        Picasso.with(mContext).load          (item.getImage())
                             .error         (R.drawable.placeholder)
                             .placeholder   (R.drawable.placeholder)
                             .into(holder.getImgThumbnail());
        holder.getLblTitle().setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return (mPhotoList != null ? mPhotoList.size() : 0);
    }


    public void loadDataNew(List<FlickrItem> newPhotos){
        mPhotoList = newPhotos;
        notifyDataSetChanged();
    }

    public FlickrItem getPhoto(int position){
        return mPhotoList!=null ? mPhotoList.get(position) : null;
    }

}
