package com.example.willgunby.flickrbrowser;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by willgunby on 03/04/15.
 */
public class ImageViewHolder extends RecyclerView.ViewHolder {

    private ImageView mImgThumbnail;
    private TextView mLblTitle;

    public ImageViewHolder(View itemView) {
        super(itemView);
        setImgThumbnail((ImageView) itemView.findViewById(R.id.imgThumbnail));
        setLblTitle((TextView) itemView.findViewById(R.id.lblTitle));
    }


    public ImageView getImgThumbnail() {
        return mImgThumbnail;
    }

    private void setImgThumbnail(ImageView imgThumbnail) {
        mImgThumbnail = imgThumbnail;
    }

    public TextView getLblTitle() {
        return mLblTitle;
    }

    private void setLblTitle(TextView lblTitle) {
        mLblTitle = lblTitle;
    }
}
