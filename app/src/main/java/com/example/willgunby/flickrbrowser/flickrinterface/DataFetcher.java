package com.example.willgunby.flickrbrowser.flickrinterface;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by willgunby on 29/03/15.
 */



public abstract class DataFetcher{
    protected abstract void executionDidComplete(Boolean success) ;
    private static final String LOG_TAG = DataFetcher.class.getSimpleName();

    AsyncFlickrAPIResponse getResponseHandler() {
        return mResponseHandler;
    }

    public enum DownloadStatus{
        IDLE,
        PROCESSING,
        NOT_INITIALISED,
        FAILED_OR_EMPTY,
        OK
    }
    private final AsyncFlickrAPIResponse mResponseHandler;

    private String mRawUrl;
    private String mData;
    private DownloadStatus mStatus;

    String getData()              { return mData;          }
    public DownloadStatus getStatus()    { return mStatus;        }
    void setRawUrl(String rawUrl) { this.mRawUrl = rawUrl; }
    String getRawUrl()            { return mRawUrl;        }

    DataFetcher(AsyncFlickrAPIResponse responseHandler) {
        mStatus = DownloadStatus.IDLE;
        this.mResponseHandler = responseHandler;
    }

    public void reset(){
        mRawUrl = null;
        mData = null;
        mStatus = DownloadStatus.IDLE;
    }

    void fetchDataFromWeb(){
        mStatus = DownloadStatus.PROCESSING;
        Log.d(LOG_TAG,"Processing URL: "+ mRawUrl);
        FetcherTask fetcherTask = new FetcherTask();
        fetcherTask.execute(mRawUrl);
    }

    private class FetcherTask extends AsyncTask<String, Void, String> {

        protected void onPostExecute(String webData) {
            DataFetcher.this.mData = webData;
            if (DataFetcher.this.mData == null) {
                if (mRawUrl == null) {
                    mStatus = DownloadStatus.NOT_INITIALISED;
                } else {
                    mStatus = DownloadStatus.FAILED_OR_EMPTY;
                }
                executionDidComplete(false);
            } else {
                mStatus = DownloadStatus.OK;
                executionDidComplete(true);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection conn = null;
            BufferedReader reader = null;
            if (params == null) return null;

            try {
                URL url = new URL(mRawUrl);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                InputStream response = conn.getInputStream();

                if (response == null) return null;
                StringBuffer buffer = new StringBuffer();
                reader = new BufferedReader(new InputStreamReader(response));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                    buffer.append("\n");
                }
                return buffer.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error", e);
            } finally {
                if (conn != null) conn.disconnect();
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing connection", e);
                    }

                }
            }
            return null;
        }
    }
}
