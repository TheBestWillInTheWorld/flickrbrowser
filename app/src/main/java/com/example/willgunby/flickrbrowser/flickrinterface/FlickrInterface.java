package com.example.willgunby.flickrbrowser.flickrinterface;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by willgunby on 01/04/15.
 */
public class FlickrInterface extends DataFetcher {
    private static final String LOG_TAG = FlickrInterface.class.getSimpleName();

    private final String mBaseUrl;

    public List<FlickrItem> getPhotos() {
        return mPhotos;
    }

    private void setPhotos(ArrayList<FlickrItem> photos) {
        mPhotos = photos;
    }

    public enum TagMode{
        //fancy enum to allow us to store an alternative representation for each Enum value
        ALL ("all") ,
        ANY ("any");

        private final String mDisplayAs;
        private TagMode(String displayAs){
            this.mDisplayAs = displayAs;
        }
        public final String display(){
            return mDisplayAs;
        }
    }

    private FlickrInterface(String baseUrl, AsyncFlickrAPIResponse responseHandler, TagMode tagMode, String... tags) {
        super(responseHandler);
        this.mBaseUrl = baseUrl;
        setupUri(tagMode, tags);
    }
    public FlickrInterface(String baseUrl, AsyncFlickrAPIResponse responseHandler){
        super(responseHandler);
        this.mBaseUrl = baseUrl;
    }

    public void doSearch(TagMode tagMode, String... tags){
        setupUri(tagMode, tags);
        doSearch();
    }
    void doSearch(){
        fetchDataFromWeb();
    }

    private void setupUri(TagMode tagMode, String... tags){
        final String tagsParam              = "tags";
        final String tagModeParam           = "tagmode";
        final String formatParam            = "format";
        final String noJsonCallbackParam    = "nojsoncallback";

        Uri target = Uri.parse(mBaseUrl).buildUpon()
                .appendQueryParameter(formatParam          , "json"            )
                .appendQueryParameter(noJsonCallbackParam  , "1"               )
                .appendQueryParameter(tagModeParam         , tagMode.display() )
                .appendQueryParameter(tagsParam            , tagsToString(tags)).build();

        setRawUrl(target.toString());
        Log.v(LOG_TAG, "URL Set: "+ getRawUrl());
    }

    private String tagsToString(String... tags){
        StringBuilder result = new StringBuilder();
        for(String tag : tags) {
            result.append(tag);
            result.append(",");
        }
        return result.length() > 0 ? result.substring(0, result.length() - 1): "";
    }

    private ArrayList<FlickrItem> mPhotos;

    @Override
    protected void executionDidComplete(Boolean success){
        if (!success) {
            Log.e(LOG_TAG,"Error fetching web data: " + getStatus().name());
            this.getResponseHandler().asyncResult(this, success);
            return;
        }
        parseJSONStringToObjects(getData());
        this.getResponseHandler().asyncResult(this, success);
    }

    private void parseJSONStringToObjects(String jsonString){
        final String jsonItems       = "items"       ;
        final String jsonTitle       = "title"       ;
        final String jsonAuthor      = "author"      ;
        final String jsonAuthorId    = "author_id"   ;
        final String jsonLink        = "link"        ;
        final String jsonTags        = "tags"        ;
        final String jsonMedia       = "media"       ;
        final String jsonPhotoUrl    = "m"           ;

        setPhotos(new ArrayList<FlickrItem>());

        try {
            final JSONObject data = new JSONObject(jsonString);
            final JSONArray items = data.getJSONArray(jsonItems);
            for(int i=0; i<items.length();i++){
                final JSONObject item = items.getJSONObject(i);
                final String title        = item.getString       (jsonTitle    );
                final String author       = item.getString       (jsonAuthor   );
                final String authorId     = item.getString       (jsonAuthorId );
                final String link         = item.getString       (jsonLink     );
                final String tags         = item.getString       (jsonTags     );
                final JSONObject media    = item.getJSONObject   (jsonMedia    );
                final String photoUrl     = media.getString      (jsonPhotoUrl );

                final FlickrItem photo = new FlickrItem(title, author, authorId, link, tags, photoUrl);
                getPhotos().add(photo);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



}
