package com.example.willgunby.flickrbrowser.flickrinterface;

/**
 * Created by willgunby on 18/03/15.
 *
 */

public interface AsyncFlickrAPIResponse {

    public void asyncResult(FlickrInterface sender, boolean success);

}
