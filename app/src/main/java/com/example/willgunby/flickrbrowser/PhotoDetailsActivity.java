package com.example.willgunby.flickrbrowser;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.willgunby.flickrbrowser.flickrinterface.AsyncFlickrImageVersionsResponse;
import com.example.willgunby.flickrbrowser.flickrinterface.FlickrItem;
import com.squareup.picasso.Picasso;


public class PhotoDetailsActivity extends BaseActivity implements AsyncFlickrImageVersionsResponse {
    private static final String LOG_TAG = PhotoDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_details);
        activateToolbarWithHomeEnabled();

        Intent intent = getIntent();
        FlickrItem item = (FlickrItem) intent.getSerializableExtra(PHOTO_TRANSFER);

        TextView photoTitle  = (TextView) findViewById(R.id.photo_title);
        TextView photoAuthor = (TextView) findViewById(R.id.photo_author);
        TextView photoTags   = (TextView) findViewById(R.id.photo_tags);

        photoTitle.setText ("Title: "+ item.getTitle());
        photoAuthor.setText("Author: "+ item.getAuthor());
        photoTags.setText  ("Tags: "+ item.getTags());

        item.getLargestVersion(this,this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void asyncResult(String finalUrl, boolean exists) {
        if (!exists) Log.d(LOG_TAG, "No versions of Photo found on Flickr.  Final URL - "+ finalUrl);
        ImageView image = (ImageView) findViewById(R.id.photo_image);
        Picasso.with(this).load (finalUrl)
                .error(R.drawable.placeholder)
                .placeholder    (R.drawable.placeholder)
                .into           (image);
    }
}
