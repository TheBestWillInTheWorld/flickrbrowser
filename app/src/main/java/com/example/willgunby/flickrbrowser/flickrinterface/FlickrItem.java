package com.example.willgunby.flickrbrowser.flickrinterface;

import android.content.Context;

import java.io.Serializable;

/**
 * Created by willgunby on 01/04/15.
 */
public class FlickrItem implements Serializable, AsyncFlickrImageVersionsResponse{
    public static long getSerialVersionId() {        return SERIAL_VERSION_ID;    }
    private static final long SERIAL_VERSION_ID = 1L;

    public String getTitle()    {  return mTitle;    }
    public String getAuthor()   {  return mAuthor;   }
    public String getAuthorId() {  return mAuthorId; }
    public String getLink()     {  return mLink;     }
    public String getTags()     {  return mTags;     }
    public String getImage()    {  return mImage;    }

    private final String mTitle;
    private final String mAuthor;
    private final String mAuthorId;
    private final String mLink;
    private final String mTags;
    private final String mImage;
    //private String published;
    //private String description;
    private String mLargestImageUrl;

    @Override
    public String toString() {
        return "FlickrItem{" +
                "mTitle='" + mTitle + '\'' +
                ", mAuthor='" + mAuthor + '\'' +
                ", mAuthorId='" + mAuthorId + '\'' +
                ", mLink='" + mLink + '\'' +
                ", mTags='" + mTags + '\'' +
                ", mImage='" + mImage + '\'' +
                '}';
    }

    public FlickrItem(String title, String author, String authorId, String link, String tags, String image) {
        this.mTitle = title;
        this.mAuthor = author;
        this.mAuthorId = authorId;
        this.mLink = link;
        this.mTags = tags;
        this.mImage = image;
        this.mLargestImageUrl = null;
    }


    private AsyncFlickrImageVersionsResponse mResponseHandler;
    public void getLargestVersion(Context context, AsyncFlickrImageVersionsResponse responseHandler){
        if (mLargestImageUrl!=null) {
            responseHandler.asyncResult(mLargestImageUrl, true);
        }
        else {
            mResponseHandler = responseHandler;
            FlickrImageVersions version = new FlickrImageVersions(context, this);
            version.getLargest(getImage());
        }

    }


    @Override
    public void asyncResult(String finalUrl, boolean exists) {
        mLargestImageUrl = finalUrl;
        mResponseHandler.asyncResult(finalUrl, exists);
    }
}
