package com.example.willgunby.flickrbrowser.flickrinterface;

/**
 * Created by willgunby on 05/04/15.
 */
public interface AsyncFlickrImageVersionsResponse {

        public void asyncResult(String finalUrl, boolean exists);

}
