package com.example.willgunby.flickrbrowser;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.willgunby.flickrbrowser.flickrinterface.AsyncFlickrAPIResponse;
import com.example.willgunby.flickrbrowser.flickrinterface.DataFetcher;
import com.example.willgunby.flickrbrowser.flickrinterface.FlickrInterface;
import com.example.willgunby.flickrbrowser.flickrinterface.FlickrItem;


public class MainActivity extends BaseActivity implements AsyncFlickrAPIResponse, RecyclerItemClickListener.OnItemClickListener {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private FlickrInterface mFlickr;
    private RecyclerViewAdaptor mFlickrView;
    private LinearLayoutManager mLayoutManager;

    private enum IntentType{
        SEARCH, SHOW_DETAIL
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activateToolbar();

        mLayoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);

        mFlickrView = new RecyclerViewAdaptor(this);
        recyclerView.setAdapter(mFlickrView);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this));


        mFlickr = new FlickrInterface(getApplicationContext().getString(R.string.flickr_api_base_url), this);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mFlickr.doSearch(FlickrInterface.TagMode.ALL, sharedPref.getString(FLICKR_QUERY, "just,a,random,search"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.menu_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivityForResult(intent, IntentType.SEARCH.ordinal());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void asyncResult(FlickrInterface sender, boolean success) {
        if (success) {
            for (FlickrItem photo : sender.getPhotos()) {
                Log.v(LOG_TAG, photo.toString());
            }
            setImages();
        }
    }

    private void setImages() {
        if (mFlickr.getStatus() == DataFetcher.DownloadStatus.OK) {
            mFlickrView.loadDataNew(mFlickr.getPhotos());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //don't redo the search unless you need to
    }


    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "Normal tap", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PhotoDetailsActivity.class);
        intent.putExtra(PHOTO_TRANSFER, mFlickrView.getPhoto(position));
        startActivityForResult(intent, IntentType.SHOW_DETAIL.ordinal());
        //TODO:persist scroll position - the photo details generally results in this view being disposed
    }

    @Override
    public void onItemLongClick(View view, int position) {
        Toast.makeText(this, "Long tap", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==IntentType.SEARCH.ordinal())  {
            Toast.makeText(this,"Search!",Toast.LENGTH_SHORT).show();

            //optionally save the search once completed
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPref.edit().putString(FLICKR_QUERY, data.getStringExtra(FLICKR_QUERY)).apply();

            //now we're persisting the Recycler in MainActivity, it needs the scroll position resetting after each load
            mLayoutManager.scrollToPosition(0);
            mFlickr.doSearch(FlickrInterface.TagMode.ALL, data.getStringExtra(FLICKR_QUERY));
        }
        if (requestCode==IntentType.SHOW_DETAIL.ordinal())  {
            //nothing to do
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
