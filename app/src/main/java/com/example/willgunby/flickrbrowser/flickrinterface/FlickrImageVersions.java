package com.example.willgunby.flickrbrowser.flickrinterface;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by willgunby on 05/04/15.
 */
public class FlickrImageVersions {
    private static final String LOG_TAG = FlickrImageVersions.class.getSimpleName();

    private final AsyncFlickrImageVersionsResponse mResponseHandler;
    private final Context mContext;

    public FlickrImageVersions(Context context, AsyncFlickrImageVersionsResponse responseHandler){
        mResponseHandler = responseHandler;
        mContext = context;
    }

    public void getLargest(final String baseUrl) {


        new Thread() {

            private final ArrayList<String> chars= new ArrayList<String>() {{
                add("a"); add("b");  add("c"); add("d"); add("e"); add("m"); add("n");
            }};

            public void run() {
                String url = "";
                for (String suffixChar : chars){
                    url = baseUrl.replaceAll("(_[a-z])?(\\.\\w{3}\\w?$)","_"+ suffixChar +"$2");
                    if (testUrl(url)){
                        Log.d(LOG_TAG, "Found URL - "+ url);
                        passResultToHandler(url, true);
                        return;
                    }
                }
                Log.d(LOG_TAG, "Failed to find URL - "+ url);
                passResultToHandler(url, false);
            }

            private void passResultToHandler(final String url, final boolean exists){
                //ensures control is passed back to the handler on the main thread of the context passed in on the constructor
                Handler mainHandler = new Handler(mContext.getMainLooper());
                Runnable myRunnable = new Runnable(){
                    @Override
                    public void run() {
                        mResponseHandler.asyncResult(url, exists);
                    }
                };
                mainHandler.post(myRunnable);
            }


            private boolean testUrl(String url){
                try {
                    Log.d(LOG_TAG, "Checking URL - "+ url);
                    HttpURLConnection.setFollowRedirects(false);
                    HttpURLConnection con =  (HttpURLConnection) new URL(url).openConnection();
                    con.setRequestMethod("HEAD");
                    Log.d(LOG_TAG, "URL: " + url + " - HTTP " + con.getResponseCode() + " - " + con.getResponseMessage());

                    return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.e(LOG_TAG, "Error while checking Url "+ url +" - "+ e.toString());
                }
                return false;
            }


        }.start();

    }
}
